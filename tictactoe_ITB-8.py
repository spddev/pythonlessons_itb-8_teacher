# Игра Крестики-нолики (консольная)

# Функции игры
def default():
    # Вывод строки-приглашения по умолчанию
    print("\nДобро пожаловать! Давай сыграем в крестики-нолики\n")


# Вывод правил игры Крестики-нолики
def rules():
    print("Игровое поле будет выглядеть следующим образом!\n")
    print("Позиции на игровом поле соответствуют цифрам на цифровой клавиатуре компьютера\n")
    print("7 | 8 | 9 ")
    print("----------")
    print("4 | 5 | 6 ")
    print("----------")
    print("1 | 2 | 3 ")
    print("\nВам нужно выбрать позицию путём ввода цифр от 1 до 9.\n")


def play():
    # Спрашиваем игрока, готов ли он играть
    return input("\nГотовы ли Вы начать игру? Введите [Д]а или [Н]ет\t").upper().startswith('Д')


def names():
    # Задание имён игроков
    p1_name = input("\nВведите имя игрока 1: \t").capitalize()
    p2_name = input("\nВведите имя игрока 2: \t").capitalize()
    return (p1_name, p2_name)


def choice():
    # Ввод выбранных значений игроками
    p1_choice = ' '
    p2_choice = ' '
    while p1_choice != 'X' or p1_choice != 'O':
        p1_choice = input(f"\nВыберите Х или O\t")[0].upper()

        if p1_choice == 'X' or p1_choice == 'O':
            break
        print('НЕПРАВИЛЬНЫЙ ВВОД! Повторите снова!')
    # Анализ ввода данных игроками
    if p1_choice == 'X':
        p2_choice = 'O'
    elif p1_choice == 'O':
        p2_choice = 'X'

    return(p1_choice, p2_choice)

# Выбор кто будет ходить первым
def first_player():
    import random
    return random.choice((0, 1))

# Отображение игрового поля
def display_board(board, avail):
    print("    " + " {} | {} | {} ".format(board[7], board[8], board[9]) + "         " + " {} | {} | {} ".format(avail[7], avail[8], avail[9]))
    print("    " + "--------------" + "         " + "--------------")
    print("    " + " {} | {} | {} ".format(board[4], board[5], board[6]) + "         " + " {} | {} | {} ".format(avail[4], avail[5], avail[6]))
    print("    " + "--------------" + "         " + "--------------")
    print("    " + " {} | {} | {} ".format(board[1], board[2], board[3]) + "         " + " {} | {} | {} ".format(avail[1], avail[2], avail[3]))


def player_choice(board, name, choice):
    position = 0
    while position not in [1, 2, 3, 4, 5, 6, 7, 8, 9] or not space_check(board, position):
        position = int(input(f"{name} ({choice}), выберите следующую позицию: (1 - 9) \t"))
        if position not in [1, 2, 3, 4, 5, 6, 7, 8, 9] or position == " ":
            print("\nНЕПРАВИЛЬНЫЙ ВВОД! Повторите снова!")
        print("\n")
        return position

def space_check(board, position):
    # Проверяем: занята ли позиция или свободна
    return board[position] == " "

def place_marker(board, avail, choice, position):
    # Заменяем/переписываем значение на игровом поле нужным нам символом
    board[position] = choice
    avail[position] = ' '


# Условия победы
def win_check(board, choice):
    # Если одно из условий ниже выполняется, то игрок выигрывает
    # ПРОВЕРКА ПО ГОРИЗОНТАЛЯМ
    return (
        (board[7] == choice and board[8] == choice and board[9] == choice)  # 7-8-9
      or (board[4] == choice and board[5] == choice and board[6] == choice)  # 4-5-6
      or (board[1] == choice and board[2] == choice and board[3] == choice)  # 1-2-3
    # ПРОВЕРКА ПО ВЕРТИКАЛЯМ
      or (board[1] == choice and board[4] == choice and board[7] == choice)  # 1-4-7
      or (board[2] == choice and board[5] == choice and board[8] == choice)  # 2-5-8
      or (board[3] == choice and board[6] == choice and board[9] == choice)  # 3-6-9
    # ПРОВЕРКА ПО ДИАГОНАЛЯМ
      or (board[7] == choice and board[5] == choice and board[3] == choice)  # 7-5-3
      or (board[1] == choice and board[5] == choice and board[9] == choice)  # 1-5-9
    )


# ИИ игры "Крестики-нолики"
def comp_ai(board, name, choice):
    position = 0
    posibilities = [x for x, letter in enumerate(board) if letter == ' ' and x != 0 ]
    for let in ['X','O']:
        for i in posibilities:
            board_copy = board[:]
            board_copy[i] = let
            if win_check(board_copy, let):
                position = i
                return position
    open_corners = [x for x in posibilities if x in [1, 3, 7, 9]]
    # Проверка на свободные углы игрового поля
    if len(open_corners) > 0:
        position = selectRandom(open_corners)
        return position
    # Проверка на свободный центр игрового поля
    if 5 in posibilities:
        position = 5
        return position
    # Проверка на свободные стороны игрового поля
    open_edges = [x for x in posibilities if x in [2, 4, 6, 8]]
    if len (open_edges) > 0:
        position = selectRandom(open_edges)
        return position


def selectRandom(board):
    import random
    ln = len(board)
    r = random.randrange(0, ln)
    return board[r]


def delay(mode):
    if mode == 2:
        import time
        time.sleep(2)


def replay():
    return input("Сыграем снова? Введите [Д]а или [Н]ет: ").lower().startswith('д')

def full_board_check(board):
    # Проверка на заполненность игрового поля для ситуации "НИЧЬЯ"
    for i in range(1, 10):
        if space_check(board, i):
            return False
    return True




default()
rules()


#### ОСНОВНОЙ ИГРОВОЙ ЦИКЛ ####
while True:
    # Создание пустого игрового поля
    theBoard = [' '] * 10

    # Создание списка всех доступных позиций игрового поля
    available = [str(num) for num in range(0, 10)]

    # Вывод режимов игры
    print("\n[0]. Игрок против компьютера\n")
    print("[1]. Игрок против игрока\n")
    print("[2]. Компьютер против компьютера\n")
    mode = int(input("\nВыберите один из режимов игры.Введите [0] - [2]: \n"))
    delay(mode)
    if mode == 0:
        p1_name = input("\nВведите имя игрока, который будет играть против компьютера:\t").capitalize()
        p2_name = "Computer"
        # Спрашиваем чем будет играть игрок Х или О
        p1_choice, p2_choice = choice()
        print(f"\n{p1_name}:", p1_choice)
        print(f"{p2_name}:", p2_choice)

    elif mode == 1:
        p1_name, p2_name = names()
        p1_choice, p2_choice = choice()
        print(f"\n{p1_name}: ", p1_choice)
        print(f"{p2_name}: ", p2_choice)
    elif mode == 2:
        p1_name = "Computer1"
        p2_name = "Computer2"
        p1_choice, p2_choice = "X","O"
    else:
        print("НЕПРАВИЛЬНЫЙ ВВОД! Повторите снова!")

    # Отображаем игроку кто ходит первым
    if first_player():
        turn = p2_name
    else:
        turn = p1_name

    print(f"\n{turn}, будет ходить первым!")

    # Спрашиваем игрока, готов ли он начать игру, выводим True или False
    if mode == 2:
        ent = input("\nЭто будет быстро! Нажмите ENTER для начала игры.")
        play_game = 1
    else:
        play_game = play()

    while play_game:
        ##################
        # ХОД ИГРОКА 1
        if turn == p1_name:
            # Отображаем игровое поле
            display_board(theBoard, available)
            if mode != 2:
                position = player_choice(theBoard, p1_name, p1_choice)
            else:
                position = comp_ai(theBoard, p1_name, p1_choice)
                print(f"\n{p1_name} ({p1_choice}) выполнил ход в позицию {position}\n")
            # Заменяет " " - символ на позиции *position* символом *p1_choice* в списке *theBoard*
            place_marker(theBoard, available, p1_choice, position)

            # Проверка на выигрыш игрока 1 после очередного хода
            if win_check(theBoard, p1_choice):
                display_board(theBoard, available)
                print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
                if mode:
                    print(f"\n\nПОЗДРАВЛЯЕМ! {p1_name} ПОБЕДИЛ!!!")
                else:
                    print("\n\nПОБЕДИЛ КОМПЬЮТЕР!!!\n\n")
                print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
                play_game = False
            else:
                # Проверка на заполненность игрового поля (НИЧЬЯ)
                if full_board_check(theBoard):
                    display_board(theBoard, available)
                    print('~~~~~~~~~~~~~~~~~~')
                    print('\nНИЧЬЯ!!!\n')
                    print('~~~~~~~~~~~~~~~~~~')
                    break
                else:
                    turn = p2_name  # передаём ход игроку 2

        # ХОД ИГРОКА 2
        elif turn == p2_name:
            # Отображаем игровое поле
            display_board(theBoard, available)
            if mode == 1:
                position = player_choice(theBoard, p2_name, p2_choice)
            else:
                position = comp_ai(theBoard, p2_name, p2_choice)
                print(f"\n{p2_name} ({p2_choice}) выполнил ход в позицию {position}\n")
            place_marker(theBoard, available, p2_choice, position)

            # Проверка на выигрыш игрока 2 после очередного хода
            if win_check(theBoard, p2_choice):
                display_board(theBoard, available)
                print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
                if mode:
                    print(f"\n\nПОЗДРАВЛЯЕМ! {p2_name} ПОБЕДИЛ!!!")
                else:
                    print("\n\nПОБЕДИЛ КОМПЬЮТЕР!!!\n\n")
                print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
                play_game = False
            else:
                # Проверка на заполненность игрового поля (НИЧЬЯ)
                if full_board_check(theBoard):
                    display_board(theBoard, available)
                    print('~~~~~~~~~~~~~~~~~~')
                    print('\nНИЧЬЯ!!!\n')
                    print('~~~~~~~~~~~~~~~~~~')
                    break
                else:
                    turn = p1_name  # Передаём ход игроку 1

    if replay():
        continue
    else:
        break


